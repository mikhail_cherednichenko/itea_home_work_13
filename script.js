//створюємо масив
const styles = ["Джаз", "Блюз"];

document.write(`Масив: "${styles}"<br>`);
document.write(`<hr>`);

//додаємо елемент в кінець
let temp = "Рок-н-рол"
styles.push(temp);

document.write(`Доданий елемент: "${temp}"<br>`);
document.write(`Отриманий масив: "${styles}"<br>`);
document.write(`<hr>`);

//вираховуємо середину масива та замінюємо елемент
let middle = parseInt(styles.length / 2);

temp = "Класика"

document.write(`Елемент всередені: "${styles[middle]}" замінили на: "${temp}"<br>`);

styles[middle] = temp;

document.write(`Отриманий масив: "${styles}"<br>`);
document.write(`<hr>`);

//видаляємо початковий елемент
temp = styles.shift();
document.write(`Видалений елемент: "${temp}"<br>`);
document.write(`Отриманий масив: "${styles}"<br>`);
document.write(`<hr>`);

//додаємо елементи в початок масиву
temp = ["Реп", "Реггі"];
document.write(`Додаємо в початок: "${temp}"<br>`);

styles.unshift(temp);
document.write(`Отриманий масив: "${styles}"<br>`);
document.write(`<hr>`);